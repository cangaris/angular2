import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appBold]'
})
export class BoldDirective {

  @HostBinding('class.bold') classBold: boolean = false;

  @HostListener('mouseenter') mouseenter () {
    this.classBold = true;
  }

  @HostListener('mouseleave') mouseleave () {
    this.classBold = false;
  }
}
