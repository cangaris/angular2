import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../services/user.service';
import { PasswordValidator } from '../validators/password-validator';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  form: FormGroup | undefined;
  user: User | undefined;

  constructor(
    private userService: UserService,
    private router: Router,
    private fb: FormBuilder,
    private passValidator: PasswordValidator,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const userId = this.activatedRoute.snapshot.params.id;
    if (userId) {
      this.userService.getUser(userId)
        .subscribe(user => this.setUserFormData(user));
    }

    const nameValidation = [Validators.required, Validators.pattern('[A-Z][a-z]{2,20}')];
    const passValidation = [Validators.required, Validators.minLength(5), Validators.maxLength(255)];
    this.form = this.fb.group({
      id: '',
      image: '',
      firstName: ['', nameValidation],
      lastName: ['', nameValidation],
      age: ['', [Validators.required, Validators.min(18), Validators.max(120)]],
      salary: ['', [Validators.required, Validators.min(3_000), Validators.max(50_000)]],
      password: ['', passValidation],
      passwordConfirm: ['', passValidation],
      email: ['', [Validators.required, Validators.email, Validators.minLength(5), Validators.maxLength(50)]],
      creditCardNo: '',
    }, {
      validators: this.passValidator.validate
    });
  }

  get image(): FormControl {
    return this.form?.get('image') as FormControl;
  }

  get firstName(): FormControl {
    return this.form?.get('firstName') as FormControl;
  }

  get lastName(): FormControl {
    return this.form?.get('lastName') as FormControl;
  }

  get age(): FormControl {
    return this.form?.get('age') as FormControl;
  }

  get salary(): FormControl {
    return this.form?.get('salary') as FormControl;
  }

  get password(): FormControl {
    return this.form?.get('password') as FormControl;
  }

  get passwordConfirm(): FormControl {
    return this.form?.get('passwordConfirm') as FormControl;
  }

  get email(): FormControl {
    return this.form?.get('email') as FormControl;
  }

  get creditCardNo(): FormControl {
    return this.form?.get('creditCardNo') as FormControl;
  }

  formData() {
    if (this.form?.valid) {
      const fn = this.user ?
        this.userService.updateUser(this.form.value) :
        this.userService.addUser(this.form.value);
      fn.subscribe(
        () => this.router.navigate(['/users']),
        (errorResponse: HttpErrorResponse) => alert(errorResponse.error.message),
      );
    }
  }

  private setUserFormData(user: User): void {
    this.user = user;
    this.form?.patchValue({
      id: user.id,
      image: user.image,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      age: user.age,
      salary: user.salary,
      creditCardNo: user.creditCardNo,
    });
  }
}
