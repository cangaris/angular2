import { Component, Input, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit, OnDestroy {
  @Input() title = 'lubię placki';
  @Input() firstName = 'Jan';
  @Input() lastName = '----';

  constructor() {
    console.log('constructor', this.firstName)
  }

  ngOnInit(): void {
    console.log('ngOnInit', this.firstName)
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroy', this.firstName)
  }
}
