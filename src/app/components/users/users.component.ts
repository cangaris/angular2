import { Component, Output, EventEmitter, Input, OnChanges, SimpleChanges, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {

  users: User[] = [];
  @Output() selectedUserEmitter = new EventEmitter<User | null>();

  constructor(private userService: UserService) {
  }

  deleteItem(index: number, user: User): void {
    this.userService.deleteUser(user)
      .subscribe(() => this.users.splice(index, 1));
  }

  setIsExpanded(selectedIndex: number, user: User, isExpanded: boolean): void {
    this.selectedUserEmitter.emit(isExpanded ? user : null);
    this.users.forEach((user, index) => user.isExpanded = selectedIndex === index);
    user.isExpanded = isExpanded;
  }

  ngOnInit(): void {
    this.userService.getUsers()
      .subscribe(users => this.users = users);
  }
}
