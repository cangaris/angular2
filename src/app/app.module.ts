import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './components/app/app.component';
import { TestComponent } from './components/test/test.component';

import localePl from '@angular/common/locales/pl';
import localeExtraPl from '@angular/common/locales/extra/pl';
import { MyUppercasePipe } from './pipes/my-uppercase.pipe';
import { CreditCardPipe } from './pipes/credit-card.pipe';
import { MyDirectiveDirective } from './directives/my-directive.directive';
import { BoldDirective } from './directives/bold.directive';
import { UsersComponent } from './components/users/users.component';
import { AddUserComponent } from './components/add-user/add-user.component';
import { IsValidDirective } from './directives/is-valid.directive';
import { ErrorLabelComponent } from './components/error-label/error-label.component';
import { FormDirective } from './directives/form.directive';

registerLocaleData(localePl, localeExtraPl);

const routes: Routes = [
  {path: 'add-user', component: AddUserComponent},
  {path: 'user/:id', component: AddUserComponent},
  {path: 'users', component: UsersComponent },
];

@NgModule({
  declarations: [AppComponent, TestComponent, MyUppercasePipe, CreditCardPipe, MyDirectiveDirective, BoldDirective, UsersComponent, AddUserComponent, IsValidDirective, ErrorLabelComponent, FormDirective],
  imports: [BrowserModule, RouterModule.forRoot(routes), FormsModule, ReactiveFormsModule, HttpClientModule],
  providers: [
    { provide: LOCALE_ID, useValue: "pl" }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
