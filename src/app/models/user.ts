export interface User {
  id: number;
  image: string;
  firstName: string;
  lastName: string;
  email: string;
  age: number;
  salary: number;
  creditCardNo: string;
  createdAt: Date;
  isExpanded?: boolean;
  isInside?: boolean;
}
