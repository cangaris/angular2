import { Directive, HostBinding, HostListener, Input, OnInit } from '@angular/core';
import { FormControl, FormControlDirective } from '@angular/forms';
import { ControlStatus } from '../models/control-status';

@Directive({
  selector: '[appIsValid]'
})
export class IsValidDirective implements OnInit {

  @HostBinding('class.is-valid') isValid: boolean = false;
  @HostBinding('class.is-invalid') isInvalid: boolean = false;
  private control!: FormControl;

  constructor(private fcd: FormControlDirective) {}

  ngOnInit(): void {
    this.control = this.fcd.control;
    this.control.statusChanges.subscribe((value: ControlStatus) => this.checkControl(value));
  }

  @HostListener('focus') onFocus() {
    this.control.markAsTouched();
    this.checkControl(this.control.status as ControlStatus);
  }

  private checkControl(value: ControlStatus) {
    if (this.control.touched || this.control.dirty) {
      switch (value) {
        case ControlStatus.VALID:
          this.isValid = true;
          this.isInvalid = false;
          break;
        case ControlStatus.INVALID:
          this.isValid = false;
          this.isInvalid = true;
          break;
      }
    }
  }
}
