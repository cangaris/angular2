import { Component } from '@angular/core';
import { User } from '../../models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  selectedUserName: string | undefined;

  catchUser(user: User | null): void {
    this.selectedUserName = user?.firstName;
  }
}
