import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-error-label',
  templateUrl: './error-label.component.html',
  styleUrls: ['./error-label.component.css'],
})
export class ErrorLabelComponent implements OnInit {

  @Input() control!: FormControl;

  constructor() {}

  ngOnInit(): void {}
}
