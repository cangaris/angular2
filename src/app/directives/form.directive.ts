import { ContentChildren, Directive, HostListener, QueryList } from '@angular/core';
import { IsValidDirective } from './is-valid.directive';

@Directive({
  selector: '[appForm]'
})
export class FormDirective {

  @ContentChildren(IsValidDirective, {descendants: true}) isValidDirectives!: QueryList<IsValidDirective>;

  @HostListener('ngSubmit') ngSubmit() {
    this.isValidDirectives.forEach(isValid => isValid.onFocus());
  }
}
