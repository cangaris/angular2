import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'creditCard'
})
export class CreditCardPipe implements PipeTransform {

  // 4323 4255 1111 9086 -> 4323 **** **** 9086
  transform(value: string, ...args: string[]): string {
    if (!value) {
      return "---";
    }
    const arr = value.split(" ");
    const stars = args[0].repeat(4);
    arr[1] = stars;
    arr[2] = stars;
    return arr.join(" ");
  }
}
