import { Injectable } from '@angular/core';
import { FormGroup, ValidationErrors, Validator } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class PasswordValidator implements Validator {

  validate(group: FormGroup): ValidationErrors | null {
    const password = group.get('password')?.value;
    const passwordConfirm = group.get('passwordConfirm')?.value;
    return password === passwordConfirm ? null : { passwordsNotSame: true };
  }
}
